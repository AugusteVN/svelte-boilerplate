import App from './App.svelte';

const containerSizes = ['xs', 'sm', 'md', 'lg', 'xl'];

const app = new App({
	target: document.body,
	props: {
		name: 'Svelte Boilerplate',
		author: 'AVN',
		containerSize: containerSizes[3]
	}
});

export default app;