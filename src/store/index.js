import { writable, readable } from 'svelte/store';
/**
 * Svelte store.
 * Define subscriptable store objects here.
 * Source: https://www.youtube.com/watch?v=PyU7TNsStyg
 */

export const exampleStore = writable(false);